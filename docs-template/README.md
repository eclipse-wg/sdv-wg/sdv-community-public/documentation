<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/github_username/repo_name">
    <img src="../README-templates/images/logo.png" alt="Logo" width="200px">
  </a>

<h3 align="center">Project Documentation</h3>

  <p align="center">
    Starter template for developer documentation
    <br />
    Based on <a href="https://docusaurus.io/"><strong>Docusaurus »</strong></a>
    <br />
    <br />
    <a href="https://github.com/github_username/repo_name">View Demo</a>
    ·
    <a href="https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-community-public/documentation/-/issues">Report Issue</a>
    ·
    <a href="https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-community-public/sdv-wg-board/-/boards">Request Feature</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About the Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About the Project

This is a starter for documentation for an Eclipse SDV project.

Documentation is essential for open source adoption and community contributions. 

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- GETTING STARTED -->
## Getting Started



### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* yarn
  ```sh
  yarn
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-community-public/documentation.git
   ```
3. Local development
   ```sh
   yarn start
   ```
4. Build
   ```sh
   yarn build
   ```

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- CONTACT -->
## Contact

For assistance in the documentation process please contact:

Craig Frayne - craig.frayne@eclipse-foundatation.com

Project Link: https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-community-public/documentation/-/tree/main/docs-template?ref_type=heads

<p align="right">(<a href="#readme-top">back to top</a>)</p>



